import { defineStore } from "pinia"
import store from "@/store"
import cache from "@/utils/cache"
import { useAccountLoginApi, useLogoutApi, useMobileLoginApi } from "@/api/auth"
import { useUserInfoApi } from "@/api/sys/user"
import { useAuthorityListApi } from "@/api/sys/menu"

export const agentStatus = defineStore('agentStatus', {
  state: () => ({
    authDialog: false,
    loginDialog: false,
    registerDialog: false,
    loadingStateConfig: false,
    loadingStateUserInfo: false,
    accountDialog: false,
    settingDialog: false
  }),
  actions: {
    setSettingDialog: (data: any) => {
      this.settingDialog = data
    },
    setAccountDialog: (data) => {
      this.accountDialog = data
    },
    setAuthDialog: (data) => {
      this.authDialog = data
    },
    setLoginDialog: (loginDialog) => {
      this.loginDialog = loginDialog
    },
    setRegisterDialog: (registerDialog) => {
      this.registerDialog = registerDialog
    },
    setLoadingStateConfig: (data) => {
      this.loadingStateConfig = data
    },
    setLoadingStateUserInfo: (data) => {
      this.loadingStateUserInfo = data
    },
    clearDialog: () => {
      this.authDialog = false
      this.loginDialog = false
      this.registerDialog = false
      this.accountDialog = false
      this.settingDialog = false
    }
  }
})

