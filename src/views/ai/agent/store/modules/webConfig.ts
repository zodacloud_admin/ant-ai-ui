import api from '@/views/ai/agent/utils/request/BaseRequest'
import { defineStore } from "pinia"
import store from '@/store'

export const webConfig = defineStore('webConfig', {
  state: () => ({
    resourceMain: {},
    resourcePromotionConfig: {},
    resourceAccountConfig: {}
  }),
  mutations: {
  },
  actions: {
    setResourceMain(val: {}){
      this.resourceMain = val
    },
    setResourcePromotionConfig(val: {}) {
      this.resourceAccountConfig = val
    },
    setResourceAccountConfig(val: {}){
      this.resourceAccountConfig = val
    },
    FlushWebConfigMain() {
      store.agentSettings.loadingStateConfig = true
      return new Promise((resolve, reject) => {
        api.get('/module/system/baseresourceconfig/clientConfig').then(res => {
          this.setResourceMain(res.data.resourceMain)
          this.setResourcePromotionConfig(res.data.resourcePromotionConfig)
          this.setResourceAccountConfig(res.data.resourceAccountConfig)
          store.userSocket.ConnectUserSocket()
          resolve()
        })
      })
    }

  }
})
